export default {
  ADD: 'Ekle',
  ADD_MODAL_TITLE: 'Yeni Ekle',
  TITLE: 'Başlık',
  TITLE_MESSAGE: 'Lütfen başlık giriniz.',
  DESCRIPTION: 'Açıklama',
  DESCRIPTION_MESSAGE: 'Lütfen açıklama giriniz.',
  PHOTO: 'Resim',
  PHOTO_PLACEHOLDER: 'Tıkla ya da Resmi alana sürükle bırak',
  PHOTO_MESSAGE: 'Lütfen resim ekleyiniz.'
};
