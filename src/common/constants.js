export const URL_ENDPOINTS = {
  CAKES_GET: '/api/products',
  LOGIN_POST: '/auth/login'
};

export const URL_HOST = 'http://ec2-3-83-223-160.compute-1.amazonaws.com:3030';

export const IMAGE_HOST = 'http://ec2-3-83-223-160.compute-1.amazonaws.com:3030/static/';
