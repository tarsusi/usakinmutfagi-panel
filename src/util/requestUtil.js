import axios from 'axios';
import { URL_HOST } from 'common/constants';
import eventEmitter from 'util/eventEmitter';

class RequestUtil {
  constructor() {
    this.axios = axios.create({
      baseURL: URL_HOST,
      headers: {
        common: {
          'Access-Control-Allow-Origin': '*',
          Accept: 'application/json, text/plain, */*'
        }
      }
    });

    this.axios.interceptors.request.use((req) => {
      req.headers.Authorization = `Bearer ${localStorage.getItem('token')}`;

      return req;
    });
  }

  onError = (err) => {
    if (err && err.response && err.response.status === 401 && err.response.statusText === 'Unauthorized') {
      eventEmitter.emit('401');
    }

    return err;
  };

  get = (url, params) => {
    return this.axios
      .get(url, { params })
      .then((response) => response.data)
      .catch((err) => err);
  };

  post = (url, params) => {
    return this.axios
      .post(url, params)
      .then((response) => response.data)
      .catch(this.onError);
  };

  put = (url, params) => {
    return this.axios
      .put(url, params)
      .then((response) => response.data)
      .catch(this.onError);
  };

  delete = (url, params) => {
    return this.axios
      .delete(url, params)
      .then((response) => response.data)
      .catch(this.onError);
  };
}

export default new RequestUtil();
