import React from 'react';
import { render } from 'react-dom';
import { BrowserRouter } from 'react-router-dom';

import './app.less';

import App from './components/app/App';

const Router = () => (
  <BrowserRouter>
    <App />
  </BrowserRouter>
);

render(Router(), document.getElementById('app'));
