import React, { Component } from 'react';
import { Form, Icon, Input, Button, message } from 'antd';
import requestUtil from 'util/requestUtil';
import { withRouter } from 'react-router-dom';
import { URL_ENDPOINTS } from 'common/constants';

class LoginComponent extends Component {
  handleSubmit = (e) => {
    e.preventDefault();

    this.props.form.validateFields((err, { email, password }) => {
      if (!err) {
        requestUtil.post(URL_ENDPOINTS.LOGIN_POST, { email, password }).then(({ success, ...otherFields }) => {
          if (success) {
            message.success('Başarılıyla Giriş yapıldı!');
            localStorage.setItem('token', otherFields.token);

            this.props.history.push('/kekler');
          } else {
            message.error(otherFields.message);
          }
        });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    return (
      <div className="login-container">
        <Form onSubmit={this.handleSubmit} className="login-form">
          <Form.Item className="form-title">Usak'ın Mutfağı Panel</Form.Item>
          <Form.Item>
            {getFieldDecorator('email', {
              rules: [{ required: true, message: 'Lüften email adresinizi giriniz!' }]
            })(<Input prefix={<Icon type="user" />} placeholder="Email" />)}
          </Form.Item>
          <Form.Item>
            {getFieldDecorator('password', {
              rules: [{ required: true, message: 'Lütfen şifrenizi giriniz!' }]
            })(<Input prefix={<Icon type="lock" />} type="password" placeholder="Password" />)}
          </Form.Item>
          <Form.Item>
            <Button type="primary" htmlType="submit" className="login-form-button">
              Giriş
            </Button>
          </Form.Item>
        </Form>
      </div>
    );
  }
}

export default withRouter(Form.create({ name: 'login_form' })(LoginComponent));
