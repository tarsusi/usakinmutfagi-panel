import React from 'react';

import CookingImage from 'images/cooking-icon.png';

const MyProfile = () => (
    <div className="my-profile-container">
        <img src={CookingImage} />
        Profilim hazırlanıyor..
    </div>
);

export default MyProfile;