import React from 'react';

import CookingImage from 'images/cooking-icon.png';

const Orders = () => (
    <div className="orders-container">
        <img src={CookingImage} />
        Siparişlerim hazırlanıyor..
    </div>
);

export default Orders;