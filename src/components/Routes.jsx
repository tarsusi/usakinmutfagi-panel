import React from 'react';
import { Switch, Redirect, Route } from 'react-router-dom';

import Login from 'components/login/Login';
import Products from 'components/products/Products';
import MyProfile from 'components/my-profile/MyProfile';
import Contact from 'components/contact/Contact';

const PrivateRoute = ({ component: Component, ...rest }) => (
  <Route
    {...rest}
    render={(props) => (localStorage.getItem('token') ? <Component {...props} /> : <Redirect to="/login" />)}
  />
);

const Routes = () => (
  <Switch>
    <Route
      exact
      path="/"
      render={() => (localStorage.getItem('token') ? <Redirect to="/kekler" /> : <Redirect to="/login" />)}
    />
    <Route
      path="/login"
      render={() => (localStorage.getItem('token') ? <Redirect to="/kekler" /> : <Login />)}
    />
    <PrivateRoute path="/kekler" component={(props) => <Products {...props} productType="kekler" />} />
    <PrivateRoute path="/kurabiyeler" component={(props) => <Products {...props} productType="kurabiyeler" />} />
    <PrivateRoute path="/pogacalar" component={(props) => <Products {...props} productType="pogacalar" />} />
    <PrivateRoute path="/pastalar" component={(props) => <Products {...props} productType="pastalar" />} />
    <PrivateRoute path="/tatlilar" component={(props) => <Products {...props} productType="tatlilar" />} />
    <PrivateRoute path="/sutlu-tatlilar" component={(props) => <Products {...props} productType="sutluTatlilar" />} />
    <PrivateRoute path="/salatalar" component={(props) => <Products {...props} productType="salatalar" />} />
    <PrivateRoute path="/yemekler" component={(props) => <Products {...props} productType="yemekler" />} />
    <PrivateRoute path="/organik-urunler" component={(props) => <Products {...props} productType="organikUrunler" />} />
    <PrivateRoute path="/blog" component={MyProfile} />
    <PrivateRoute path="/videolar" component={MyProfile} />
    <Route path="/iletisim" component={Contact} />
  </Switch>
);

export default Routes;
