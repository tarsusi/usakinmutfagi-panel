import React from 'react';

import CookingImage from 'images/cooking-icon.png';

const OrganicCrops = () => (
    <div className="organic-crops-container">
        <img src={CookingImage} />
        Organik ürünlerimiz hazırlanıyor..
    </div>
);

export default OrganicCrops;