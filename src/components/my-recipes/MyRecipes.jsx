import React from 'react';

import CookingImage from 'images/cooking-icon.png';

/**
 * Nefis yemek tarifleri uygulamasına göz at
 * En üstte
 *  Kaç kişilik
 *  Hazırlama Süresi
 *  Pişirme Süresi
 *  
 * Malzeme listesi
 * Hazırlanışı (madde madde)
 */
const MyRecipes = () => (
    <div className="my-recipes-container">
        <img src={CookingImage} />
        Tariflerim hazırlanıyor..
    </div>
);

export default MyRecipes;