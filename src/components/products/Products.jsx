import React, { Component } from 'react';

import { Button, Card, message, Popconfirm } from 'antd';

import isEmpty from 'lodash/isEmpty';

import { IMAGE_HOST, URL_ENDPOINTS } from 'common/constants';

import requestUtil from 'util/requestUtil';

import AddProductModal from './addProductModal';

import WORDS from 'common/words';

const { Meta } = Card;

class Products extends Component {
  constructor(props) {
    super(props);

    this.state = {
      defaultParams: null,
      isAddModalOpen: false,
      products: []
    };
  }

  componentDidMount() {
    this.getProducts();
  }

  addProduct = (productParams) => {
    const params = {
      ...productParams,
      type: this.props.productType
    };

    let editedParams;

    this.setState(
      (prevState) => {
        editedParams = prevState.defaultParams;

        return {
          defaultParams: null,
          isAddModalOpen: false
        };
      },
      () => {
        if (!isEmpty(editedParams)) {
          requestUtil
            .put(`${URL_ENDPOINTS.CAKES_GET}/${editedParams.id}`, params)
            .then(({ success }) => {
              if (success) {
                message.success('Başarılıyla Güncellendi!');
                this.getProducts();
              }
            })
            .catch(() => {
              message.error('Bir sorunla karşılaşıldı!');
            });
        } else {
          requestUtil
            .post(URL_ENDPOINTS.CAKES_GET, params)
            .then(({ success }) => {
              if (success) {
                message.success('Başarılıyla Eklendi!');
                this.getProducts();
              }
            })
            .catch(() => {
              message.error('Bir sorunla karşılaşıldı!');
            });
        }
      }
    );
  };

  onDelete = (product) => {
    requestUtil
      .delete(`${URL_ENDPOINTS.CAKES_GET}/${product._id}`)
      .then((response) => {
        if (response.success) {
          message.success('Başarılı bir şekilde silinmiştir');
        } else {
          message.error(response.error);
        }
      })
      .catch(() => message.error('Bir sorunla karşılaşıldı!'))
      .finally(() => {
        this.getProducts();
      });
  };

  getProducts = () => {
    requestUtil
      .get(URL_ENDPOINTS.CAKES_GET, { type: this.props.productType })
      .then(({ products, success }) => {
        if (success) {
          this.setState({
            defaultParams: null,
            products
          });
        }
      })
      .catch((error) => {
        console.log(error);
      });
  };

  render() {
    return (
      <div className="products-container">
        {this.state.products.map((product) => (
          <div className="product" key={product._id}>
            <Card
              className="product-card"
              cover={<img alt="example" src={`${IMAGE_HOST}${product.photo}`} />}
              extra={
                <div className="extra-buttons">
                  <Popconfirm
                    title="Silmek istediğinize emin misiniz?"
                    onConfirm={() => this.onDelete(product)}
                    okText="Evet"
                    cancelText="Hayır"
                  >
                    <Button className="product-delete-button" icon="delete" size="small" type="ghost" />
                  </Popconfirm>
                  <Button
                    className="product-edit-button"
                    icon="edit"
                    onClick={() =>
                      this.setState({
                        isAddModalOpen: true,
                        defaultParams: {
                          id: product._id,
                          title: product.title,
                          type: product.type,
                          description: product.description,
                          photo: product.photo
                        }
                      })
                    }
                    size="small"
                    type="ghost"
                  />
                </div>
              }
              hoverable
              title={product.title}
            >
              <Meta description={product.description} />
            </Card>
          </div>
        ))}
        <div className="product-adder-button">
          <Button
            className="product-item-button"
            icon="plus"
            onClick={() => this.setState({ defaultParams: null, isAddModalOpen: true })}
            shape="circle"
            size="large"
            type="primary"
          />
          <AddProductModal
            defaultParams={this.state.defaultParams}
            isOpen={this.state.isAddModalOpen}
            modalTitle={WORDS.ADD_MODAL_TITLE}
            onOk={this.addProduct}
            onCancel={() => this.setState({ defaultParams: null, isAddModalOpen: false })}
            productType={this.props.productType}
          />
        </div>
      </div>
    );
  }
}

export default Products;
