import React, { Component } from 'react';

import { Form, Icon, Input, Modal, Upload } from 'antd';

import isEmpty from 'lodash/isEmpty';

import { URL_HOST } from 'common/constants';

import requestUtil from 'util/requestUtil';

import WORDS from 'common/words';

const { TextArea } = Input;

class AddProductModal extends Component {
  constructor(props) {
    super(props);

    this.state = {
      fileList: [],
      photo: '',
      previewVisible: false,
      previewImage: null
    };
  }

  componentDidUpdate(prevProps) {
    if (isEmpty(prevProps.defaultParams) && !isEmpty(this.props.defaultParams)) {
      this.setInitialValues(this.props.defaultParams);
    }
  }

  setInitialValues = (params) => {
    this.props.form.setFieldsValue({
      title: params.title,
      description: params.description
    });
  };

  afterClose() {
    this.setState((prevState) => {
      let newState = {};

      if (prevState.photo) {
        requestUtil.delete('/upload', {
          data: {
            fileName: prevState.photo,
            type: this.props.productType
          }
        });

        newState = {
          photo: '',
          fileList: [],
          previewVisible: false,
          previewImage: null
        };
      }

      return newState;
    });
  }

  handlePreview = (file) => {
    this.setState({
      previewImage: file.url || file.thumbUrl,
      previewVisible: true
    });
  };

  handleCancel = () => this.setState({ previewVisible: false });

  handleChange = ({ file, fileList }) => {
    if (file.status === 'done') {
      this.setState({
        photo: file.response.fileName
      });
    }

    if (file.status === 'removed') {
      this.setState({
        photo: ''
      });
    }

    this.setState({ fileList });
  };

  handleRemove = () => {
    return requestUtil.delete('/upload', {
      data: {
        fileName: this.state.photo,
        type: this.props.productType
      }
    });
  };

  onOk = () => {
    this.props.form.validateFields(['title', 'description', 'photo'], (errors, values) => {
      if (!errors) {
        this.setState((prevState) => {
          this.props.onOk({
            ...values,
            photo: prevState.photo
          });

          return {
            fileList: [],
            photo: ''
          };
        });
      }
    });
  };

  render() {
    const { getFieldDecorator } = this.props.form;

    const formItemLayout = {
      labelCol: { span: 4 },
      wrapperCol: { span: 18 }
    };

    return (
      <Modal
        afterClose={() => this.afterClose()}
        centered
        destroyOnClose
        forceRender
        onOk={this.onOk}
        onCancel={this.props.onCancel}
        title={this.props.modalTitle}
        visible={this.props.isOpen}
      >
        <Form className="product-modal" layout="horizontal">
          <Form.Item label={WORDS.TITLE} {...formItemLayout}>
            {getFieldDecorator('title', {
              rules: [{ required: true, message: WORDS.TITLE_MESSAGE }]
            })(<Input placeholder={WORDS.TITLE} />)}
          </Form.Item>
          <Form.Item label={WORDS.DESCRIPTION} {...formItemLayout}>
            {getFieldDecorator('description', {
              rules: [{ required: true, message: WORDS.DESCRIPTION_MESSAGE }]
            })(<TextArea placeholder={WORDS.DESCRIPTION} rows={3} />)}
          </Form.Item>
          <Form.Item {...formItemLayout} label={WORDS.PHOTO}>
            <div className="dropbox">
              {getFieldDecorator('photo', {
                rules: [{ required: true, message: WORDS.PHOTO_MESSAGE }]
              })(
                <Upload
                  action={`${URL_HOST}/upload?type=${this.props.productType}`}
                  listType="picture-card"
                  fileList={this.state.fileList}
                  onPreview={this.handlePreview}
                  onRemove={this.handleRemove}
                  onChange={this.handleChange}
                >
                  {this.state.fileList.length === 0 && (
                    <div>
                      <Icon type="plus" />
                      <div className="ant-upload-text">Upload</div>
                    </div>
                  )}
                </Upload>
              )}
            </div>
          </Form.Item>
        </Form>
        <Modal visible={this.state.previewVisible} footer={null} onCancel={this.handleCancel}>
          <img alt="example" style={{ width: '100%' }} src={this.state.previewImage} />
        </Modal>
      </Modal>
    );
  }
}

export default Form.create({ name: 'add_product_form' })(AddProductModal);
