import React from 'react';

import CookingImage from 'images/cooking-icon.png';

const Promotions = () => (
    <div className="promotions-container">
        <img src={CookingImage} />
        Promosyonlar / Kampanyalar hazırlanıyor..
    </div>
);

export default Promotions;