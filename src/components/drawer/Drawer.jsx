import React from 'react';

import { Button, Drawer } from 'antd';

import MyLeftMenu from 'components/left-menu/LeftMenu';

const MyDrawer = (props) => (
  <Drawer
    closable={false}
    placement="left"
    onClose={props.onClose}
    title={
      <div className="drawer-title-container">
        <div className="drawer-header-title">Hoşgeldiniz</div>
        <Button className="header-toggle-button" icon="bars" size="large" type="default" onClick={props.onClick} />
      </div>
    }
    visible={props.isMenuOpen}
  >
    <MyLeftMenu onMenuItemClick={props.onClose} />
  </Drawer>
);

export default MyDrawer;
