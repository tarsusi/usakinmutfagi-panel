import React from 'react';

import CookingImage from 'images/cooking-icon.png';

const Contact = () => (
    <div className="contact-container">
        <img src={CookingImage} />
        İletişim sayfası hazırlanıyor..
    </div>
);

export default Contact;