import React from 'react';
import { withRouter } from 'react-router-dom';

import { message } from 'antd';
import eventEmitter from 'util/eventEmitter';

import LeftMenu from 'components/left-menu/LeftMenu';
import MyDrawer from 'components/drawer/Drawer';
import MyHeader from 'components/header/Header';

import Routes from 'components/Routes';
import { Layout } from 'antd';

const { Header, Sider, Content } = Layout;

class App extends React.Component {
  componentDidMount() {
    eventEmitter.on('401', () => {
      localStorage.setItem('token', '');
      message.info('Oturum Sonlandırıldı');

      this.props.history.push('/');
    });
  }

  state = {
    isMenuOpen: false
  };

  onClose = () => {
    this.setState({
      isMenuOpen: false
    });
  };

  toggleMenu = () => {
    this.setState((prevState) => ({
      isMenuOpen: !prevState.isMenuOpen
    }));
  };

  checkForLoggedIn = () => localStorage.getItem('token');

  render() {
    return (
      <Layout className="app-container">
        {this.checkForLoggedIn() && (
          <Header>
            <MyHeader toggleMenu={this.toggleMenu} />
          </Header>
        )}
        <Layout>
          {this.checkForLoggedIn() && (
            <>
              <Sider width={225} theme="light">
                <LeftMenu />
              </Sider>
              <MyDrawer isMenuOpen={this.state.isMenuOpen} onClose={this.onClose} onClick={this.onClose} />
            </>
          )}
          <Content className="content-container">
            <Routes />
          </Content>
        </Layout>
        )
      </Layout>
    );
  }
}

export default withRouter(App);
