const path = require('path');
const webpack = require('webpack');
const HtmlWebPackPlugin = require('html-webpack-plugin');
const CleanWebpackPlugin = require('clean-webpack-plugin');
const UglifyJSPlugin = require('uglifyjs-webpack-plugin');
const ScriptExtHtmlWebpackPlugin = require('script-ext-html-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');

const PROJECT_NAME = process.env.npm_package_name;
const PROJECT_VERSION = `${process.env.npm_package_version}.${process.env.npm_package_build}`;
const BUNDLE_NAME = `${PROJECT_NAME}-${PROJECT_VERSION}`;

module.exports = (env) => {
  const BUILD_MODE = env === 'dev' ? 'development' : 'production';
  const DEV_TOOL = env === 'dev' ? 'inline-source-map' : 'source-map';
  const HOT_RELOAD = env === 'dev' ? [new webpack.HotModuleReplacementPlugin()] : [];
  const UGLIFY_JS_PLUGIN =
    env === 'dev'
      ? []
      : [
          new webpack.DefinePlugin({
            // <-- key to reducing React's size
            'process.env': {
              NODE_ENV: 'production'
            }
          }),
          new UglifyJSPlugin(), //minify everything
          new webpack.optimize.AggressiveMergingPlugin() //Merge chunks
        ];

  return {
    devtool: DEV_TOOL,
    entry: path.join(__dirname, 'src/app.js'),
    mode: BUILD_MODE,
    module: {
      rules: [
        {
          test: /\.(js|jsx)$/,
          exclude: /node_modules/,
          loader: 'babel-loader'
        },
        {
          test: /\.(png|jpg|gif)$/,
          use: [
            {
              loader: 'file-loader',
              options: {}
            }
          ]
        },
        {
          test: /\.css$/,
          use: [
            {
              loader: 'style-loader' // creates style nodes from JS strings
            },
            {
              loader: MiniCssExtractPlugin.loader,
              options: {
                // you can specify a publicPath here
                // by default it use publicPath in webpackOptions.output
                publicPath: '../'
              }
            },
            'css-loader'
          ]
        },
        {
          test: /\.less$/,
          use: [
            {
              loader: 'style-loader' // creates style nodes from JS strings
            },
            {
              loader: 'css-loader' // translates CSS into CommonJS
            },
            {
              loader: 'less-loader' // compiles Less to CSS
            }
          ]
        }
      ]
    },
    resolve: {
      modules: [
        path.resolve(__dirname, 'node_modules'),
        path.resolve(__dirname, 'src'),
        path.resolve(__dirname, 'assets')
      ],
      extensions: ['.css', '.js', '.jsx']
    },
    output: {
      filename: `${BUNDLE_NAME}.js`,
      path: __dirname + '/dist'
    },
    plugins: [
      new HtmlWebPackPlugin({
        template: './src/index.html',
        filename: './index.html'
      }),
      ...UGLIFY_JS_PLUGIN,
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/),
      new CleanWebpackPlugin(['dist']),
      new ScriptExtHtmlWebpackPlugin({
        defaultAttribute: 'defer'
      }),
      new MiniCssExtractPlugin({
        // Options similar to the same options in webpackOptions.output
        // both options are optional
        filename: '[name].css',
        chunkFilename: '[id].css'
      }),
      ...HOT_RELOAD
    ],
    devServer: {
      contentBase: './src',
      hot: true,
      historyApiFallback: true
    },
    optimization: {
      splitChunks: {
        chunks: 'async',
        minSize: 30000,
        maxSize: 0,
        minChunks: 1,
        maxAsyncRequests: 5,
        maxInitialRequests: 3,
        automaticNameDelimiter: '~',
        name: true,
        cacheGroups: {
          vendors: {
            test: /[\\/]node_modules[\\/]/,
            priority: -10
          },
          default: {
            minChunks: 2,
            priority: -20,
            reuseExistingChunk: true
          }
        }
      }
    }
  };
};
